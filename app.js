"use strict"

// getText();
// function getText(){
//     console.log("text");
// }
//
// getTextWithCustomText("Какой-то текст");
// function getTextWithCustomText(Peremennayatext){
//     console.log(Peremennayatext);
// }
//
// let text = "Какой-то текст";
// getTextWithCustomText(text);
// function getTextWithCustomText(Peremennayatext){
//     console.log(Peremennayatext);
// }



// функция старта игры
function start_game() {
    set_line();
    // set_color_board();
}
//
// function set_color_board() {
//
// }

// функция отрисовки строк
// tr
function set_line() {
    for (let i = 0; i <= 9; i++) {
        let new_row = document.createElement('div');
        new_row.className = "chess_row chess_row_" + i;
        // Document.getElementById("chess_board").innerHTML('div');
        document.getElementById('chess_board').appendChild(new_row);
        // вот тут мы создали только что 1 строку и у нас есть переменная i равная 0 -> первой строке
        //какую переменную надо передать в set_col()?
        //мы запускаем функцию создания строк с цифрой нашей строки
        set_col(i);
    }
}
// функция отрисовки колонок которая может принять число, в нашем случае это номер созданной строки.
// td
function set_col(row = 0) {
    // получив нужную нам строку допустим это первая строка с цифрой 0
    // запускаем цикл создания колонок
    for (let i = 0; i <= 9; i++) {
        let new_col = document.createElement('div');
        let color;
        // если i делится на 2 без остатка то число чётное значит ячейка чёрная.
        //создаём переменную char которая у нас либо 0 либо 1
        let char;
        // если строка row чётная то char равна 0
        if(row % 2 == 0){
            char = 0
        }else {
            char = 1;
        }
        // проверяем i неравна нашей переменой char то красим ячёку в чёрный цвет
        if (i % 2 != char){
            color = " black_cell"
        }else {
            color = " white_cell"
        }
        // тут мы создаем класс для накшей колноки допустим это первая колонка с классом <div class="chess_col chess_col_0(айдишник нашей строки)_0(айдишник нашей колонки);
        new_col.className = "chess_col chess_col_"+ row + "_" + i + color;
        // Document.getElementById("chess_board").innerHTML('div');
        // создаем строку .chess_row_1 по которой будем искать нашу строку и в неё добавлять наши колонки
        let curr_row = '.chess_row_' + row;
        document.querySelector(curr_row).appendChild(new_col);
    }
}